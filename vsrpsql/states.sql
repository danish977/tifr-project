-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:23 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `aoi1_id` int(11) NOT NULL,
  `aoi1_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dept_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`aoi1_id`, `aoi1_name`, `dept_id`, `status`) VALUES
(1, 'Sun and Star', 1, 1),
(2, 'Matter and Chemistry', 1, 1),
(3, 'Formation of star', 1, 1),
(4, 'Gamma Ray', 1, 1),
(5, 'Neutron Star', 1, 1),
(6, 'Active Neuclie', 1, 1),
(7, 'Cosomology and Large', 1, 1),
(8, 'Instrumentation', 1, 1),
(9, 'Stellar', 1, 1),
(10, 'Pulsars', 1, 1),
(11, 'Galaxy cluster', 2, 1),
(12, 'Gravitational Lensing', 2, 1),
(1652, 'Hussain', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`aoi1_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `aoi1_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1653;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
