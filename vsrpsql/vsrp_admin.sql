-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:24 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `vsrp_admin`
--

CREATE TABLE `vsrp_admin` (
  `a_main_id` int(11) NOT NULL COMMENT 'Main ID',
  `a_mainname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Main Name',
  `a_mainemail` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Main Email',
  `a_password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Password',
  `a_confpass` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Confirm Password',
  `a_cdate` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Creation date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vsrp_admin`
--
ALTER TABLE `vsrp_admin`
  ADD PRIMARY KEY (`a_main_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vsrp_admin`
--
ALTER TABLE `vsrp_admin`
  MODIFY `a_main_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Main ID';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
