-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:24 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `temp_vsrp_student`
--

CREATE TABLE `temp_vsrp_student` (
  `s_fname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'First Name',
  `s_mname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Middle Name',
  `s_lname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Last Name',
  `s_email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Email',
  `s_title` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mobile No.',
  `s_unders` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'M/F',
  `s_posts` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Date',
  `s_college` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Address',
  `s_dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Department',
  `s_part` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Area of Interest 1',
  `s_password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Area of Interest 2',
  `s_id` int(11) NOT NULL COMMENT 'ID',
  `hash` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'hash funtion',
  `password` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'random password generation',
  `email_verify` text COLLATE utf8_unicode_ci NOT NULL,
  `email_time` time NOT NULL,
  `email_addtime` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
