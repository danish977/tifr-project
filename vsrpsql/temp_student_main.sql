-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:24 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `temp_student_main`
--

CREATE TABLE `temp_student_main` (
  `s_email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'email of student',
  `s_name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'full name of student ',
  `s_gender` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'gender of student',
  `s_dob` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'dob of  student',
  `s_birth` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'birthplace of student ',
  `s_nationality` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nationality of student ',
  `s_photo` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'photo of student ',
  `s_add1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 1',
  `s_add2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address2',
  `s_add3` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 3',
  `s_add4` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 4',
  `s_cadd1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'corresponding address',
  `s_cadd2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'corresponding address',
  `s_tel` int(10) NOT NULL COMMENT 'telephone ',
  `s_mobno` int(10) NOT NULL COMMENT 'mobile no ',
  `s_desc` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'description of student ',
  `s_eng` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'engineering degree',
  `s_odeg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other degrees',
  `s_osubject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other subjects',
  `s_deptchoice` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'department choice',
  `s_aor1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'area of interest 1',
  `s_aor2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'area of interest 2',
  `s_dyi` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'description',
  `s_subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'selected subject',
  `s_colin` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'selected college or institiutions',
  `s_yopg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'year of post graduate ',
  `s_seldeg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'select degree',
  `s_college` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'college ',
  `s_univ` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'university',
  `s_year` int(4) NOT NULL COMMENT 'year',
  `s_class` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'class',
  `s_marks` int(11) NOT NULL COMMENT 'cgpa/marks',
  `s_urank` int(11) NOT NULL COMMENT 'university rank ',
  `s_sub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'subjects',
  `s_odeg1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other degree',
  `s_ocollege` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other colleges',
  `s_ouniv` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other university',
  `s_oyear` int(4) NOT NULL COMMENT 'other year ',
  `s_oclass` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other class',
  `s_omarks` int(11) NOT NULL COMMENT 'other marks ',
  `s_ourank` int(11) NOT NULL COMMENT 'other university rank ',
  `s_osub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other subjects ',
  `s_expdegree` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other expected degree',
  `s_expmonth` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'month and year ',
  `s_pubdetails` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'publications',
  `s_rexp` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'research experience',
  `s_r1name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 name',
  `s_r1des` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 designation',
  `s_r1dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 detpartment',
  `s_r1inst` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 institution',
  `s_r1tel` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 telephone',
  `s_r1mobno` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 mobile no ',
  `s_r1email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 email',
  `s_r1add` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 address',
  `s_r2name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 name',
  `s_r2des` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 designation',
  `s_r2dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 detpartment',
  `s_r2inst` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 institution',
  `s_r2tel` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 telephone',
  `s_r2mobno` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 mobile no ',
  `s_r2email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 email',
  `s_r2add` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 address',
  `s_addinfo` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'add information',
  `s_main_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `temp_student_main`
--

INSERT INTO `temp_student_main` (`s_email`, `s_name`, `s_gender`, `s_dob`, `s_birth`, `s_nationality`, `s_photo`, `s_add1`, `s_add2`, `s_add3`, `s_add4`, `s_cadd1`, `s_cadd2`, `s_tel`, `s_mobno`, `s_desc`, `s_eng`, `s_odeg`, `s_osubject`, `s_deptchoice`, `s_aor1`, `s_aor2`, `s_dyi`, `s_subject`, `s_colin`, `s_yopg`, `s_seldeg`, `s_college`, `s_univ`, `s_year`, `s_class`, `s_marks`, `s_urank`, `s_sub`, `s_odeg1`, `s_ocollege`, `s_ouniv`, `s_oyear`, `s_oclass`, `s_omarks`, `s_ourank`, `s_osub`, `s_expdegree`, `s_expmonth`, `s_pubdetails`, `s_rexp`, `s_r1name`, `s_r1des`, `s_r1dept`, `s_r1inst`, `s_r1tel`, `s_r1mobno`, `s_r1email`, `s_r1add`, `s_r2name`, `s_r2des`, `s_r2dept`, `s_r2inst`, `s_r2tel`, `s_r2mobno`, `s_r2email`, `s_r2add`, `s_addinfo`, `s_main_id`) VALUES
('marufshaikh211@gmail.com', 'bruce thomas wayne', 'm', '2018-10-02', 'd', 'sdf', '', '3/6,Devraj Ramraj Chawl, Opp. B.M.C. BANK, S.V. Road, Jogeshwari (West)', 'Maharashtra', '<br /><b>Notice</b>:  Undefined variable: s_add3 in <b>/storage/ssd2/061/6717061/public_html/aamir/stdform1.php</b> on line <b>775</b><br />', 'Mumbai', '3/6,Devraj Ramraj Chawl, Opp. B.M.C. BANK, S.V. Road, Jogeshwari (West)', '<br /><b>Notice</b>:  Undefined variable: s_cadd2 in <b>/storage/ssd2/061/6717061/public_html/aamir/stdform1.php</b> on line <b>783</b><br />', 234567, 2147483647, '', '', '', '', 'Physcis', 'A', 'A', '', '', '', '', 'A', 'qwerty', 'qwerty', 2015, 'First Year', 65, 1, 'qwerty', 'A', 'qwerty', 'asdfg', 2015, 'First Yaer', 67, 1, 'qwerty', 'ghjk', '2018-08', '<br />\r\n<b>Notice</b>:  Undefined variable: s_pubdetails1 in <b>/storage/ssd2/061/6717061/public_html/tifr_vsrp/html/main_html/stdform/stdform1.php</b> on line <b>1256</b><br />\r\n', 'research', 'name', 'designation', 'BIOLOGY', 'name', '1234567890', '1234567890', 'danishbihari48@gmail.com', 'rdtyagskk', 'nam2', 'des2', 'dep2', 'inst2', '123456789', '123456789', 'tanzilworld@gmail.com', 'research', 'ad onfp', 11);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `temp_student_main`
--
ALTER TABLE `temp_student_main`
  ADD PRIMARY KEY (`s_main_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `temp_student_main`
--
ALTER TABLE `temp_student_main`
  MODIFY `s_main_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
