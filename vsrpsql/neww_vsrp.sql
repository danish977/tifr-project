-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:32 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_aoi`
--

CREATE TABLE `admin_aoi` (
  `a_id` int(11) NOT NULL,
  `a_aoiname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'AOI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_aoi`
--

INSERT INTO `admin_aoi` (`a_id`, `a_aoiname`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'A'),
(4, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `admin_date`
--

CREATE TABLE `admin_date` (
  `a_id` int(10) NOT NULL COMMENT 'Main ID',
  `a_program` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Programme',
  `a_year` int(4) NOT NULL,
  `a_officialname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Official Name',
  `a_officialemail` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Official Email',
  `a_uniquestart` int(15) NOT NULL COMMENT 'Unique Start',
  `a_vsrpadmin` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Email',
  `a_regstartallsub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration Start for all sub',
  `a_regendallsub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration end for all sub',
  `a_regendmath` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration end for Maths',
  `a_regexpdays` int(3) NOT NULL COMMENT 'Registration Expiry days',
  `a_refendmath` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Refree Link End Math Date',
  `a_refendallsub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Refree link end for all sub date',
  `a_default` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_date`
--

INSERT INTO `admin_date` (`a_id`, `a_program`, `a_year`, `a_officialname`, `a_officialemail`, `a_uniquestart`, `a_vsrpadmin`, `a_regstartallsub`, `a_regendallsub`, `a_regendmath`, `a_regexpdays`, `a_refendmath`, `a_refendallsub`, `a_default`) VALUES
(7, 'danish', 3000, 'danish bihari', 'danish@mail.com', 3000, 'db', '11-11-2018', '24-11-2018', '24-11-2018', 0, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_department`
--

CREATE TABLE `admin_department` (
  `a_did` int(11) NOT NULL COMMENT 'Main ID',
  `a_dname` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_department`
--

INSERT INTO `admin_department` (`a_did`, `a_dname`) VALUES
(1, 'a'),
(2, 'b');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `admin_id` int(11) NOT NULL COMMENT 'Main ID',
  `admin_name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Name',
  `admin_email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Email',
  `admin_password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Password'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'Hussain Dharwala', 'dharwala.hussain@gmail.com', '123654789'),
(2, 'Maruf', 'marufshaikh211@gmail.com', 'Maruf');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `aoi2_id` int(11) NOT NULL,
  `aoi2_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dept_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`aoi2_id`, `aoi2_name`, `dept_id`, `status`) VALUES
(1, 'Sun and Star', 1, 1),
(2, 'Matter and chemistry', 1, 1),
(3, 'Stellar', 1, 1),
(4, 'Pulsars', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`dept_id`, `dept_name`, `status`) VALUES
(1, 'DDA', 1),
(2, 'DTP', 1),
(3, 'DHEP', 1),
(4, 'DNAP', 1),
(5, 'DCMPMS', 1),
(6, 'DCS', 1),
(7, 'DBS', 1),
(8, 'MATH', 1),
(9, 'STCS', 1),
(10, 'TCIS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `aoi1_id` int(11) NOT NULL,
  `aoi1_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dept_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`aoi1_id`, `aoi1_name`, `dept_id`, `status`) VALUES
(1, 'Sun and Star', 1, 1),
(2, 'Matter and Chemistry', 1, 1),
(3, 'Formation of star', 1, 1),
(4, 'Gamma Ray', 1, 1),
(5, 'Neutron Star', 1, 1),
(6, 'Active Neuclie', 1, 1),
(7, 'Cosomology and Large', 1, 1),
(8, 'Instrumentation', 1, 1),
(9, 'Stellar', 1, 1),
(10, 'Pulsars', 1, 1),
(11, 'Galaxy cluster', 2, 1),
(12, 'Gravitational Lensing', 2, 1),
(1652, 'Hussain', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_student_main`
--

CREATE TABLE `temp_student_main` (
  `s_email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'email of student',
  `s_name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'full name of student ',
  `s_gender` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'gender of student',
  `s_dob` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'dob of  student',
  `s_birth` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'birthplace of student ',
  `s_nationality` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nationality of student ',
  `s_photo` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'photo of student ',
  `s_add1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 1',
  `s_add2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address2',
  `s_add3` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 3',
  `s_add4` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 4',
  `s_cadd1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'corresponding address',
  `s_cadd2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'corresponding address',
  `s_tel` int(10) NOT NULL COMMENT 'telephone ',
  `s_mobno` int(10) NOT NULL COMMENT 'mobile no ',
  `s_desc` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'description of student ',
  `s_eng` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'engineering degree',
  `s_odeg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other degrees',
  `s_osubject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other subjects',
  `s_deptchoice` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'department choice',
  `s_aor1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'area of interest 1',
  `s_aor2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'area of interest 2',
  `s_dyi` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'description',
  `s_subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'selected subject',
  `s_colin` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'selected college or institiutions',
  `s_yopg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'year of post graduate ',
  `s_seldeg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'select degree',
  `s_college` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'college ',
  `s_univ` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'university',
  `s_year` int(4) NOT NULL COMMENT 'year',
  `s_class` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'class',
  `s_marks` int(11) NOT NULL COMMENT 'cgpa/marks',
  `s_urank` int(11) NOT NULL COMMENT 'university rank ',
  `s_sub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'subjects',
  `s_odeg1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other degree',
  `s_ocollege` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other colleges',
  `s_ouniv` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other university',
  `s_oyear` int(4) NOT NULL COMMENT 'other year ',
  `s_oclass` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other class',
  `s_omarks` int(11) NOT NULL COMMENT 'other marks ',
  `s_ourank` int(11) NOT NULL COMMENT 'other university rank ',
  `s_osub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other subjects ',
  `s_expdegree` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other expected degree',
  `s_expmonth` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'month and year ',
  `s_pubdetails` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'publications',
  `s_rexp` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'research experience',
  `s_r1name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 name',
  `s_r1des` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 designation',
  `s_r1dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 detpartment',
  `s_r1inst` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 institution',
  `s_r1tel` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 telephone',
  `s_r1mobno` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 mobile no ',
  `s_r1email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 email',
  `s_r1add` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 address',
  `s_r2name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 name',
  `s_r2des` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 designation',
  `s_r2dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 detpartment',
  `s_r2inst` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 institution',
  `s_r2tel` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 telephone',
  `s_r2mobno` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 mobile no ',
  `s_r2email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 email',
  `s_r2add` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 address',
  `s_addinfo` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'add information',
  `s_main_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `temp_student_main`
--

INSERT INTO `temp_student_main` (`s_email`, `s_name`, `s_gender`, `s_dob`, `s_birth`, `s_nationality`, `s_photo`, `s_add1`, `s_add2`, `s_add3`, `s_add4`, `s_cadd1`, `s_cadd2`, `s_tel`, `s_mobno`, `s_desc`, `s_eng`, `s_odeg`, `s_osubject`, `s_deptchoice`, `s_aor1`, `s_aor2`, `s_dyi`, `s_subject`, `s_colin`, `s_yopg`, `s_seldeg`, `s_college`, `s_univ`, `s_year`, `s_class`, `s_marks`, `s_urank`, `s_sub`, `s_odeg1`, `s_ocollege`, `s_ouniv`, `s_oyear`, `s_oclass`, `s_omarks`, `s_ourank`, `s_osub`, `s_expdegree`, `s_expmonth`, `s_pubdetails`, `s_rexp`, `s_r1name`, `s_r1des`, `s_r1dept`, `s_r1inst`, `s_r1tel`, `s_r1mobno`, `s_r1email`, `s_r1add`, `s_r2name`, `s_r2des`, `s_r2dept`, `s_r2inst`, `s_r2tel`, `s_r2mobno`, `s_r2email`, `s_r2add`, `s_addinfo`, `s_main_id`) VALUES
('marufshaikh211@gmail.com', 'bruce thomas wayne', 'm', '2018-10-02', 'd', 'sdf', '', '3/6,Devraj Ramraj Chawl, Opp. B.M.C. BANK, S.V. Road, Jogeshwari (West)', 'Maharashtra', '<br /><b>Notice</b>:  Undefined variable: s_add3 in <b>/storage/ssd2/061/6717061/public_html/aamir/stdform1.php</b> on line <b>775</b><br />', 'Mumbai', '3/6,Devraj Ramraj Chawl, Opp. B.M.C. BANK, S.V. Road, Jogeshwari (West)', '<br /><b>Notice</b>:  Undefined variable: s_cadd2 in <b>/storage/ssd2/061/6717061/public_html/aamir/stdform1.php</b> on line <b>783</b><br />', 234567, 2147483647, '', '', '', '', 'Physcis', 'A', 'A', '', '', '', '', 'A', 'qwerty', 'qwerty', 2015, 'First Year', 65, 1, 'qwerty', 'A', 'qwerty', 'asdfg', 2015, 'First Yaer', 67, 1, 'qwerty', 'ghjk', '2018-08', '<br />\r\n<b>Notice</b>:  Undefined variable: s_pubdetails1 in <b>/storage/ssd2/061/6717061/public_html/tifr_vsrp/html/main_html/stdform/stdform1.php</b> on line <b>1256</b><br />\r\n', 'research', 'name', 'designation', 'BIOLOGY', 'name', '1234567890', '1234567890', 'danishbihari48@gmail.com', 'rdtyagskk', 'nam2', 'des2', 'dep2', 'inst2', '123456789', '123456789', 'tanzilworld@gmail.com', 'research', 'ad onfp', 11);

-- --------------------------------------------------------

--
-- Table structure for table `temp_vsrp_student`
--

CREATE TABLE `temp_vsrp_student` (
  `s_fname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'First Name',
  `s_mname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Middle Name',
  `s_lname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Last Name',
  `s_email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Email',
  `s_title` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mobile No.',
  `s_unders` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'M/F',
  `s_posts` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Date',
  `s_college` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Address',
  `s_dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Department',
  `s_part` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Area of Interest 1',
  `s_password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Area of Interest 2',
  `s_id` int(11) NOT NULL COMMENT 'ID',
  `hash` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'hash funtion',
  `password` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'random password generation',
  `email_verify` text COLLATE utf8_unicode_ci NOT NULL,
  `email_time` time NOT NULL,
  `email_addtime` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vsrp_admin`
--

CREATE TABLE `vsrp_admin` (
  `a_main_id` int(11) NOT NULL COMMENT 'Main ID',
  `a_mainname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Main Name',
  `a_mainemail` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Main Email',
  `a_password` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Password',
  `a_confpass` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Confirm Password',
  `a_cdate` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Creation date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vsrp_student`
--

CREATE TABLE `vsrp_student` (
  `s_id` text NOT NULL,
  `s_fname` text NOT NULL COMMENT 'First Name',
  `s_mname` text NOT NULL COMMENT 'Middle Name',
  `s_lname` text NOT NULL COMMENT 'Last Name',
  `s_email` text NOT NULL COMMENT 'Email',
  `s_title` text NOT NULL COMMENT 'Mobile No.',
  `s_unders` text NOT NULL COMMENT 'M/F',
  `s_posts` text NOT NULL COMMENT 'Date',
  `s_college` text NOT NULL COMMENT 'Address',
  `s_dept` text NOT NULL COMMENT 'Department',
  `s_part` text NOT NULL COMMENT 'Area of Interest 1',
  `s_password` text NOT NULL COMMENT 'Area of Interest 2',
  `hash` text NOT NULL COMMENT 'hash funtion',
  `password` longtext NOT NULL COMMENT 'random password generation',
  `email_verify` text NOT NULL,
  `email_time` time NOT NULL,
  `s_incid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vsrp_student`
--

INSERT INTO `vsrp_student` (`s_id`, `s_fname`, `s_mname`, `s_lname`, `s_email`, `s_title`, `s_unders`, `s_posts`, `s_college`, `s_dept`, `s_part`, `s_password`, `hash`, `password`, `email_verify`, `email_time`, `s_incid`) VALUES
('', 'bruce', 'thomas', 'wayne', 'marufshaikh211@gmail.com', 'Dr', '1', '1', 'A', 'BIOLOGY', 'y', '', '', 'f804d21145597e42851fa736e221da3f', '', '00:00:00', 1),
('', 'bruce', 'thomas', 'wayne', '', 'Dr', '1', '1', 'A', 'CHEMISTRY', 'y', '', '', 'f804d21145597e42851fa736e221da3f', '1', '00:00:00', 2),
('', 'bruce', 'thomas', 'wayne', 'marufsh@gmail.com', 'Dr', '1', '1', 'A', 'MATHEMATICS', 'y', '', '', 'f804d21145597e42851fa736e221da3f', '', '00:00:00', 3),
('', 'Maruf', 'Majid', ' Shaikh', 'maruf.shaikh2251@gmail.com', 'Mr', '1', '1', 'A', 'PHYSICS', 'n', '', '', '', '', '00:00:00', 4),
('', 'Zaid', 'Shariq', ' Siddique', 'tifrvsrp2018@gmail.com', 'Dr', '1', '1', 'A', 'COMPUTER AND SYSTEMS SCIENCES', 'n', '', '', '1d72310edc006dadf2190caad5802983', '', '00:00:00', 5),
('', 'Tanzil ahmed', 'Abdul Aziz', ' Ansari', 'tanzilahmed.ansari7@gmail.com', 'Dr', '1', '1', 'A', 'Physcis', 'y', '', '', '57827ddd068a17ad6dfc6690962241e5', '', '00:00:00', 6),
('', 'pranav', 'ramkrishna', ' ghag', 'ghagpranav6198@gmail.com', 'Mr', '1', '1', 'A', 'Physcis', 'y', '', '', '3501672ebc68a5524629080e3ef60aef', '', '00:00:00', 7),
('', 'Naruto', 'Minato', ' Uzumaki', 'dharwala.hussain@gmail.com', 'Mr', '1', '1', 'A', 'Physcis', 'n', '', '', '7aee26c309def8c5a2a076eb250b8f36', '', '00:00:00', 8),
('', 'Aflah', 'x', ' Shaikh', 'aflahsk3@gmail.com', 'Mr', '1', '1', 'A', 'Physcis', 'y', '', '', 'eae31887c8969d1bde123982d3d43cd2', '', '00:00:00', 9);

-- --------------------------------------------------------

--
-- Table structure for table `vsrp_student_main`
--

CREATE TABLE `vsrp_student_main` (
  `s_email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'email of student',
  `s_name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'full name of student ',
  `s_gender` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'gender of student',
  `s_dob` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'dob of  student',
  `s_birth` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'birthplace of student ',
  `s_nationality` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'nationality of student ',
  `s_photo` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'photo of student ',
  `s_add1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 1',
  `s_add2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address2',
  `s_add3` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 3',
  `s_add4` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'address 4',
  `s_cadd1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'corresponding address',
  `s_cadd2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'corresponding address',
  `s_tel` int(10) NOT NULL COMMENT 'telephone ',
  `s_mobno` int(10) NOT NULL COMMENT 'mobile no ',
  `s_desc` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'description of student ',
  `s_eng` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'engineering degree',
  `s_odeg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other degrees',
  `s_osubject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other subjects',
  `s_deptchoice` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'department choice',
  `s_aor1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'area of interest 1',
  `s_aor2` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'area of interest 2',
  `s_dyi` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'description',
  `s_subject` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'selected subject',
  `s_colin` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'selected college or institiutions',
  `s_yopg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'year of post graduate ',
  `s_seldeg` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'select degree',
  `s_college` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'college ',
  `s_univ` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'university',
  `s_year` int(4) NOT NULL COMMENT 'year',
  `s_class` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'class',
  `s_marks` int(11) NOT NULL COMMENT 'cgpa/marks',
  `s_urank` int(11) NOT NULL COMMENT 'university rank ',
  `s_sub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'subjects',
  `s_odeg1` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other degree',
  `s_ocollege` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other colleges',
  `s_ouniv` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other university',
  `s_oyear` int(4) NOT NULL COMMENT 'other year ',
  `s_oclass` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other class',
  `s_omarks` int(11) NOT NULL COMMENT 'other marks ',
  `s_ourank` int(11) NOT NULL COMMENT 'other university rank ',
  `s_osub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other subjects ',
  `s_expdegree` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'other expected degree',
  `s_expmonth` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'month and year ',
  `s_pubdetails` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'publications',
  `s_rexp` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'research experience',
  `s_r1name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 name',
  `s_r1des` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 designation',
  `s_r1dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 detpartment',
  `s_r1inst` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 institution',
  `s_r1tel` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 telephone',
  `s_r1mobno` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 mobile no ',
  `s_r1email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 email',
  `s_r1add` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref1 address',
  `s_r2name` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 name',
  `s_r2des` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 designation',
  `s_r2dept` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 detpartment',
  `s_r2inst` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 institution',
  `s_r2tel` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 telephone',
  `s_r2mobno` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 mobile no ',
  `s_r2email` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 email',
  `s_r2add` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ref2 address',
  `s_addinfo` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'add information',
  `s_main_id` int(10) NOT NULL,
  `hashed_email` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vsrp_student_main`
--

INSERT INTO `vsrp_student_main` (`s_email`, `s_name`, `s_gender`, `s_dob`, `s_birth`, `s_nationality`, `s_photo`, `s_add1`, `s_add2`, `s_add3`, `s_add4`, `s_cadd1`, `s_cadd2`, `s_tel`, `s_mobno`, `s_desc`, `s_eng`, `s_odeg`, `s_osubject`, `s_deptchoice`, `s_aor1`, `s_aor2`, `s_dyi`, `s_subject`, `s_colin`, `s_yopg`, `s_seldeg`, `s_college`, `s_univ`, `s_year`, `s_class`, `s_marks`, `s_urank`, `s_sub`, `s_odeg1`, `s_ocollege`, `s_ouniv`, `s_oyear`, `s_oclass`, `s_omarks`, `s_ourank`, `s_osub`, `s_expdegree`, `s_expmonth`, `s_pubdetails`, `s_rexp`, `s_r1name`, `s_r1des`, `s_r1dept`, `s_r1inst`, `s_r1tel`, `s_r1mobno`, `s_r1email`, `s_r1add`, `s_r2name`, `s_r2des`, `s_r2dept`, `s_r2inst`, `s_r2tel`, `s_r2mobno`, `s_r2email`, `s_r2add`, `s_addinfo`, `s_main_id`, `hashed_email`) VALUES
('dharwala.hussain@gmail.com', 'Hussain Mannan Dharwala', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('marufshaikh211@gmail.com', 'bruce thomas wayne', 'm', '2018-10-02', 'd', 'sdf', 'uploads/fbd6f60978a7fad16f5898f7060e12ca.jpg', '3/6,Devraj Ramraj Chawl, Opp. B.M.C. BANK, S.V. Road, Jogeshwari (West)', 'Maharashtra', '<br /><b>Notice</b>:  Undefined variable: s_add3 in <b>/storage/ssd2/061/6717061/public_html/aamir/stdform1.php</b> on line <b>775</b><br />', 'Mumbai', '3/6,Devraj Ramraj Chawl, Opp. B.M.C. BANK, S.V. Road, Jogeshwari (West)', '<br /><b>Notice</b>:  Undefined variable: s_cadd2 in <b>/storage/ssd2/061/6717061/public_html/aamir/stdform1.php</b> on line <b>783</b><br />', 234567, 2147483647, '', '', '', '', '', '', '', '', '', '', '', 'A', 'qwerty', 'qwerty', 2015, 'First Year', 65, 1, 'qwerty', 'A', 'qwerty', 'asdfg', 2015, 'First Yaer', 67, 1, 'qwerty', 'ghjk', '2018-08', '<br />\r\n<b>Notice</b>:  Undefined variable: s_pubdetails1 in <b>/storage/ssd2/061/6717061/public_html/student/stdform/stdform1.php</b> on line <b>836</b><br />\r\n', 'research', 'name', 'designation', 'BIOLOGY', 'name', '1234567890', '1234567890', 'danishbihari48@gmail.com', 'rdtyagskk', 'nam2', 'des2', 'dep2', 'inst2', '123456789', '123456789', 'tanzilworld@gmail.com', 'research', 'ad onfp', 7, 'fbd6f60978a7fad16f5898f7060e12ca'),
('tanzilworld@gmail.com', 'Sanjay', 'MALE', '2018-09-28', 'Mumbai', 'Indian', 'uploads/3473bfbfb1c8871f6ce8271933233b8d.jpg', '17,Chahsmawala bldg', '23', 'Madanpura', 'Mumbai', '17,Chahsmawala bldg', '56', 976881362, 2147483647, 'Engineering', 'IT', 'BSCIT', 'MATHS', 'Physcis', 'A', 'A', 'hello tifr', 'Maths', 'mhsscoe', '2019', 'A', 'mhsscoe', 'mumbai', 2015, 'First Yaer', 74, 1, 'science', 'B', 'mhsscoe', 'mumbai', 2015, 'Fourth year', 74, 1, 'maths', 'BE', '2019-06', 'IEEE', 'I HAVE DONE RESEARCH IN HOME AUTOMATION \r\nCURRENTLY WORKING ON GOOGLE BUGS IN CALIFORNIA', 'sanjay', 'manager', 'PHYSICS', 'kalina', '123456', '87654', 'tanzilworld@gmail.com', 'juni masjid', 'singhania', 'GM', 'BUSINESS', 'LONDON', '65456', '3456543', 'tanzilworld@gmail.com', 'byculla', 'mscit certification done ', 8, ''),
('lio.messi@gm.com', 'Lionel', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 10, ''),
('liomessi@gmail.com', 'lionel_messi_andres', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, '', '', '', '', 0, '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 11, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_aoi`
--
ALTER TABLE `admin_aoi`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `admin_date`
--
ALTER TABLE `admin_date`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `admin_department`
--
ALTER TABLE `admin_department`
  ADD PRIMARY KEY (`a_did`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`aoi2_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`aoi1_id`);

--
-- Indexes for table `temp_student_main`
--
ALTER TABLE `temp_student_main`
  ADD PRIMARY KEY (`s_main_id`);

--
-- Indexes for table `vsrp_admin`
--
ALTER TABLE `vsrp_admin`
  ADD PRIMARY KEY (`a_main_id`);

--
-- Indexes for table `vsrp_student`
--
ALTER TABLE `vsrp_student`
  ADD PRIMARY KEY (`s_incid`);

--
-- Indexes for table `vsrp_student_main`
--
ALTER TABLE `vsrp_student_main`
  ADD PRIMARY KEY (`s_main_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_aoi`
--
ALTER TABLE `admin_aoi`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_date`
--
ALTER TABLE `admin_date`
  MODIFY `a_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Main ID', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `admin_department`
--
ALTER TABLE `admin_department`
  MODIFY `a_did` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Main ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Main ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `aoi2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6178;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `aoi1_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1653;

--
-- AUTO_INCREMENT for table `temp_student_main`
--
ALTER TABLE `temp_student_main`
  MODIFY `s_main_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vsrp_admin`
--
ALTER TABLE `vsrp_admin`
  MODIFY `a_main_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Main ID';

--
-- AUTO_INCREMENT for table `vsrp_student`
--
ALTER TABLE `vsrp_student`
  MODIFY `s_incid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
