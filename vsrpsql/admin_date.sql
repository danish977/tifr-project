-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:22 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_date`
--

CREATE TABLE `admin_date` (
  `a_id` int(10) NOT NULL COMMENT 'Main ID',
  `a_program` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Programme',
  `a_year` int(4) NOT NULL,
  `a_officialname` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Official Name',
  `a_officialemail` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Official Email',
  `a_uniquestart` int(15) NOT NULL COMMENT 'Unique Start',
  `a_vsrpadmin` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Admin Email',
  `a_regstartallsub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration Start for all sub',
  `a_regendallsub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration end for all sub',
  `a_regendmath` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Registration end for Maths',
  `a_regexpdays` int(3) NOT NULL COMMENT 'Registration Expiry days',
  `a_refendmath` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Refree Link End Math Date',
  `a_refendallsub` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Refree link end for all sub date',
  `a_default` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_date`
--

INSERT INTO `admin_date` (`a_id`, `a_program`, `a_year`, `a_officialname`, `a_officialemail`, `a_uniquestart`, `a_vsrpadmin`, `a_regstartallsub`, `a_regendallsub`, `a_regendmath`, `a_regexpdays`, `a_refendmath`, `a_refendallsub`, `a_default`) VALUES
(7, 'danish', 3000, 'danish bihari', 'danish@mail.com', 3000, 'db', '11-11-2018', '24-11-2018', '24-11-2018', 0, '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_date`
--
ALTER TABLE `admin_date`
  ADD PRIMARY KEY (`a_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_date`
--
ALTER TABLE `admin_date`
  MODIFY `a_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Main ID', AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
