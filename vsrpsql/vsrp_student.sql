-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:29 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6717061_tifr_vsrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `vsrp_student`
--

CREATE TABLE `vsrp_student` (
  `s_id` text NOT NULL,
  `s_fname` text NOT NULL COMMENT 'First Name',
  `s_mname` text NOT NULL COMMENT 'Middle Name',
  `s_lname` text NOT NULL COMMENT 'Last Name',
  `s_email` text NOT NULL COMMENT 'Email',
  `s_title` text NOT NULL COMMENT 'Mobile No.',
  `s_unders` text NOT NULL COMMENT 'M/F',
  `s_posts` text NOT NULL COMMENT 'Date',
  `s_college` text NOT NULL COMMENT 'Address',
  `s_dept` text NOT NULL COMMENT 'Department',
  `s_part` text NOT NULL COMMENT 'Area of Interest 1',
  `s_password` text NOT NULL COMMENT 'Area of Interest 2',
  `hash` text NOT NULL COMMENT 'hash funtion',
  `password` longtext NOT NULL COMMENT 'random password generation',
  `email_verify` text NOT NULL,
  `email_time` time NOT NULL,
  `s_incid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vsrp_student`
--

INSERT INTO `vsrp_student` (`s_id`, `s_fname`, `s_mname`, `s_lname`, `s_email`, `s_title`, `s_unders`, `s_posts`, `s_college`, `s_dept`, `s_part`, `s_password`, `hash`, `password`, `email_verify`, `email_time`, `s_incid`) VALUES
('', 'bruce', 'thomas', 'wayne', 'marufshaikh211@gmail.com', 'Dr', '1', '1', 'A', 'BIOLOGY', 'y', '', '', 'f804d21145597e42851fa736e221da3f', '', '00:00:00', 1),
('', 'bruce', 'thomas', 'wayne', '', 'Dr', '1', '1', 'A', 'CHEMISTRY', 'y', '', '', 'f804d21145597e42851fa736e221da3f', '1', '00:00:00', 2),
('', 'bruce', 'thomas', 'wayne', 'marufsh@gmail.com', 'Dr', '1', '1', 'A', 'MATHEMATICS', 'y', '', '', 'f804d21145597e42851fa736e221da3f', '', '00:00:00', 3),
('', 'Maruf', 'Majid', ' Shaikh', 'maruf.shaikh2251@gmail.com', 'Mr', '1', '1', 'A', 'PHYSICS', 'n', '', '', '', '', '00:00:00', 4),
('', 'Zaid', 'Shariq', ' Siddique', 'tifrvsrp2018@gmail.com', 'Dr', '1', '1', 'A', 'COMPUTER AND SYSTEMS SCIENCES', 'n', '', '', '1d72310edc006dadf2190caad5802983', '', '00:00:00', 5),
('', 'Tanzil ahmed', 'Abdul Aziz', ' Ansari', 'tanzilahmed.ansari7@gmail.com', 'Dr', '1', '1', 'A', 'Physcis', 'y', '', '', '57827ddd068a17ad6dfc6690962241e5', '', '00:00:00', 6),
('', 'pranav', 'ramkrishna', ' ghag', 'ghagpranav6198@gmail.com', 'Mr', '1', '1', 'A', 'Physcis', 'y', '', '', '3501672ebc68a5524629080e3ef60aef', '', '00:00:00', 7),
('', 'Naruto', 'Minato', ' Uzumaki', 'dharwala.hussain@gmail.com', 'Mr', '1', '1', 'A', 'Physcis', 'n', '', '', '7aee26c309def8c5a2a076eb250b8f36', '', '00:00:00', 8),
('', 'Aflah', 'x', ' Shaikh', 'aflahsk3@gmail.com', 'Mr', '1', '1', 'A', 'Physcis', 'y', '', '', 'eae31887c8969d1bde123982d3d43cd2', '', '00:00:00', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vsrp_student`
--
ALTER TABLE `vsrp_student`
  ADD PRIMARY KEY (`s_incid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vsrp_student`
--
ALTER TABLE `vsrp_student`
  MODIFY `s_incid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
