<?php /*
session_start();
include("../basic_form/connect.php");

if(!empty($_SESSION['s_email'])){
header("Location: ../student/s_header.php");
die(); */ //uncomment this madarchod

?>

<!DOCTYPE html>
<html>
<head>
	<title>VSRP</title>
		 <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">


</head>
<body>

	<div class="cont">
		<div class="col-xs-12 container-fluid">
		
			<!--start of header-->
			<div class="s_header" >
	<?php

require('s_header.php');
?>
</div>
			<!--end of header-->


					<!--start of left_box-->
		<div class="left_box" >
	<?php
require('left_box.php');
?>
</div>	
					<!--end of left_box-->


			<div class="col-md-10 col-xs-12 s_main1 " id="s_main1">
<div class="content2" id="content2">
    <h5>Online System </h5>
	</div>
	<div id="content">
			<!-- #BeginEditable "content" -->
			<strong>
			<h2 class="style10"><span class="style4">How to apply</span></h2>
			</strong><p class="auto-style6"><strong><span class="style11">Students are encouraged to apply 
			online. <br>
			</span><span class="style6">
			<br>
			</span>
			
		</strong>
			<span class="style4">1. Registration includes two steps : First 
			complete basic registration step by clicking the "Apply Online" 
			button given below.<br>
			<br>
			2. During the basic registration, "Prefered Subject" indicates TIFR 
			subject area you are applying for (Physics / Chemistry / Biology / 
			Mathematics / Computer &amp; Systems Sciences).<br>
			<br>
			3. After completing basic registration, an email will be sent to you 
			with a link for advanced registration.&nbsp; Before proceeding to 
			advanced registration, please keep the following ready :<br>
			<br>
			<strong><span class="auto-style5">a) Scanned photo (.jpg file of size not exceeding 100kb ) of your 
			passport size photograph. <br>
			<br>
			b) Students can select upto 2 departments, and for each department, 
			they can fill in two areas of interest from the options available in 
			the menu. Please read the 
			</span> 
			<a class="style17" target="_parent" href="../Brochures/Brochures.htm">Information 
			Brochure</a><span class="auto-style5"> before you fill this section.<br>
			<br>
			c) A write-up of up to 400 words, describing your interest in the 
			most preferred field of study has to be provided in the appropriate 
			box. The write up should not contain special characters. In 
			particular, use of equations in write-ups is strongly discouraged.
			<br>
			<br>
			d) Names, Affiliation, Designation, Contact address (both e-mail and 
			physical address) and contact phone number of &nbsp;two referees who would be 
			filling referee reports for you. They should be teachers or persons 
			with whom you have interacted academically. <br>
			<br>
			The link for filling up the Referee Report Form online will be sent by 
			email to both Referees after complete registration.<br>
			</span>&nbsp;</strong></span></p><strong>
		</strong>
			<p class="auto-style4"><span class="style9">Students from remote areas who do not have access to 
			internet can send a request for application form along with a 
			self-addressed stamped envelope of Rs 20/- to :</span><br>
			<br>
			Superscribed (VSRP-2018)<br>
			<br>
			The Assistant Registrar (Academic)<br>
			Tata Institute of Fundamental Research<br>
			Homi Bhabha Road, Colaba<br>
			Mumbai 400005.</p>
			<p class="auto-style3"><strong>Last date for 
			application&nbsp;: <br>
			For Maths - February 28, 2018 <br>
			For all other 
			subjects - January 31, 2018</strong></p>
			<strong>
			<strong>
					</strong><strong><strong>
					</strong></strong><table style="width: 100%; height: 38px">
				<tbody><tr>
					<td style="height: 42px"></td>
			
			
			<td style="width: 151px; height: 42px">
					<em>
					<a href="http://univ.tifr.res.in/vsrp2018/registration_step_one.php" target="_blank">
					<img class="style16" height="30" src="../files/applybutton.png" width="150"></a></em><span class="style4"></span></td>
					<td style="height: 42px"></td>
				</tr>
			</tbody></table><strong><strong>
		</strong>
			
			</strong>
			<strong><strong>
			<strong>
		</strong>
		</strong>
		</strong>
			<!-- #EndEditable --></strong></div>
			</div>
		 </div>
	
	<script>





			$(document).ready(function(){

$(document).ready(function(){
    $("#nav1").click(function(){
        $("#s_main1").load("basic_registration.php");
    });
});

$(document).ready(function(){
    $("#nav2").click(function(){
        $("#s_main1").load("login_form.php");
    });
});
		
					
	});
</script>


<div class="footer">
	Copyright 2018-2019 All rights reserved. Best viewed at 1024 X 768 resolution. Browser support Mozilla Firefox 2.0 &amp; above , I.E. 7 &amp; above.(W7)
</div>

</body>
</html>