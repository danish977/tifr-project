<!DOCTYPE html>
<html>
<head>
	<title>VSRP</title>
		 <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">


</head>
<body>


<div class="col-md-2 col-xs-12" style="padding-right: 0px; padding-left:0px; overflow-y: auto; ">
<div class="left_box">
    <div class="panel-group" id="">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a href="" class="">Login Links</a><i class="fa fa-caret-down"></i>
        </h4>
      </div>
	<div class="list-group-hover sidebar-widget-1" style="padding:0px;">
        <ul class="list-unstyled" id="sidebarmenu1" >
					<li><b><a href=# id="nav1"class="list-group-item" style="color:blue"><i class="fa fa-angle-double-right"></i>New Registration (For Non-CAP)</a></b></li>
			
		    <li><a href=# id="nav2" class="list-group-item "><i class="fa fa-angle-double-right"></i> Registered Candidate Login</a></li>
			<li><a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> FC Login</a></li>
			<li><a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> ARC Login</a></li>
			<li><a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Institute Login</a></li>
			<li><a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Regional Office Login</a></li>
			<li><a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Admin Login</a></li>
		</ul>
	</div>

	</div>
	
	<div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a href="">Important Links</a><i class="fa fa-caret-down"></i>
        </h4>
      </div>

<div class="list-group-hover sidebar-widget-1">
        <ul class="list-unstyled" id="sidebarmenu1">
			<li> <a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Important Dates</a></li>
            		    <li> <a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Final Merit List For JK Candidate</a></li>

			<li> <a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Application Fee</a></li>

            <li> <a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> List of Facilitation Center's(FC)</a></li>

		   <li> <a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> List of ARC's</a></li>
			<li> <a href=# class="list-group-item "><i class="fa fa-angle-double-right"></i> Institute Wise Allotment</a></li>
			<li> <a href=# target="_blank" class="list-group-item "><i class="fa fa-angle-double-right"></i> List of Institutes Participated in CAP</a></li>
			<li><a href=# class="list-group-item " target="_blank"><i class="fa fa-angle-double-right"></i>CCVIS (Caste Certificate Verification Information System) Application</a></li>
		  <li><a href=# class="list-group-item " target="_blank"><i class="fa fa-angle-double-right"></i>Tribe Validity Certificate Registration Form</a></li>
		</ul>
    </div>

</div>
<div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a href="" class="" target="_blank">Gazetted Rules</a><i class="fa fa-caret-down"></i>
        </h4>
      </div>

		<div class="list-group-hover sidebar-widget-1">
        <ul class="list-unstyled" id="sidebarmenu1">
		

             <li><a href=# target="_blank" class="list-group-item "><i class="fa fa-angle-double-right"></i>Information Brochure</a></li>
 
 			 <li><a href=# target="_blank" class="list-group-item ">
			<i class="fa fa-angle-double-right"></i>Eligibility Criteria</a></li>
            <li><a href=# target="_blank" class="list-group-item "><i class="fa fa-angle-double-right"></i> Proforma's Formats</a></li>

			<li><a href=#  target="_blank" class="list-group-item "><i class="fa fa-angle-double-right"></i>Rules for Admissions Diploma(24/04/2017)</a></li>

            <li><a href=# target="_blank" class="list-group-item ">
			<i class="fa fa-angle-double-right"></i>Amendment Rules for Admissions Diploma (05/06/2018)</a></li>
				
		</ul>

	</div>

	</div>
</div>

</div>
</div>

</body>
</html>