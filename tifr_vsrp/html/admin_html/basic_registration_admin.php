<?php
session_start();

if(isset($_SESSION['admin_email']))
{
    
?>


<!DOCTYPE html>
<html>
<head>
	<title>VSRP</title>
		 <!-- Bootstrap -->
		 <!-- <meta name="viewport" content="width=device-width">	 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">
    
    <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
   
</head>
<body>

	
			
			<div class=" s_center">
				<div class="form" id="content_right" >

					 <form class="form-signin" method="POST" onsubmit="return validation()" action="../../process/basic_process/admin_registration.php">
					 	<div class="col-xs-12">
					 		<div class="col-xs-3 col-md-3"></div>
					 		<div class="col-xs-9 col-md-9" >
			       		  	 	<h2 class="form-signin-heading" style="font-family: times new roman"> USER REGISTRATION</h2>
			       		  	 	<h6>(BY ADMIN)</h6>
			       		  	</div>
			       		</div>
			        <!-- <div class="input-group" > -->
					
					<!-- </div> -->
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
			     Title:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<select class="fa fa-" name="s_title" required="1" >
								<option >Mr</option>
								<option >Mrs</option>
								<option >Dr</option>
								<option >Er</option>
							</select>
			 </div>
		</div><br>
	<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
			     First Name:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="s_fname" required="1"  > 
			 </div>
		</div><br>
		<div class="col-xs-12" style="padding: 10px;">
				<div class="col-md-3 col-xs-3" style="height:30px;">
					 Middle name:
		 </div> 
					 <div class="col-md-9 col-xs-9"> 
					 	  <input type="text" class="fa fa-" name="s_mname" required="1">
					 </div>
				</div><br>
				<div class="col-xs-12" style="padding: 10px;">
					<div class="col-md-3 col-xs-3" style="height:30px;">
						 Last name:
		 </div> 
					 <div class="col-md-9 col-xs-9"> 
					 	 <input type="text" class="fa fa-" name="s_lname" required="1">
					 </div>
				</div><br>
				<div class="col-xs-12" style="padding: 10px;">
					<div class="col-md-3 col-xs-3" style="height:30px;">
						 Email:
		 </div> 
					 <div class="col-md-9 col-xs-9"> 
					 	 <input type="email" class="fa fa-" name="s_email" required="1" > 
					 </div>
				</div><br>
			<!-- 	<div class="col-xs-12" style="padding: 10px;">
					<div class="col-md-3 col-xs-3" style="height:30px;">
						Phone No:
		 </div> 
					 <div class="col-md-9 col-xs-9"> 
							<input type="number" class="fa fa-" name="s_mobno" ></br><br>
					 </div>
				</div><br> -->
				<div class="col-xs-12" style="padding: 10px;">
				<div class="col-md-3 col-xs-3 s_undergraduatestudy" style="height:30px;">
					Undergraduate Study:
		 </div> 
					 <div class="col-md-9 col-xs-9"> 
							<select class="fa fa-" name="s_undergraduatestudy" required="1"  >
										<option >1</option>
										<option >2</option>
										<option >3</option>
										<option >4</option>
									</select>
					 </div>
				</div><br><br>

			<div class="col-xs-12" style="padding: 10px;">
					<div class="col-md-3 col-xs-3 s_poststudy" style="height:30px;">
						Postgraduate Study:
		 </div> 
					 <div class="col-md-9 col-xs-9"> 
							<select class="fa fa-" name="s_poststudy" required="1"  >
										<option >1</option>
										<option >2</option>
										<option >3</option>
										<option >4</option>
									</select>
					 </div>
				</div><br><br>


		<!-- <div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
				Gender:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="radio" class="fa fa-" name="s_gender" value="m">MALE</br>
					<input type="radio" class="fa fa-" name="s_gender" value="f">FEMALE</br>
					<input type="radio" class="fa fa-" name="s_gender" value="o">OTHERS</br><br>
			 </div>
		</div><br> -->
		
		<!-- <div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
				DOB:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
					<input type="date" class="fa fa-" name="s_dob"  > 
			 </div>
		</div><br> -->

		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
			     College/Institute
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<select class="fa fa-" name="s_college" required="1"  >
								<option >A</option>
								<option >B</option>
								<option >C</option>
								<option >D</option>
				</select>				
			 </div>

		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px; margin-left: -10px; margin-right: 5px;      padding-bottom: 52px;
">
				Have You participated in VSRP before?:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="radio" class="fa fa-" name="s_part" value="y" required="1">YES</br>
					<input type="radio" class="fa fa-" name="s_part" value="n" required="1">NO</br>
					<!-- <input type="radio" class="fa fa-" name="s_part" value="o">OTHERS</br><br> -->
			 </div>
		</div><br>		

		<div class="col-xs-12" style="padding: 18px; ">
			<div class="col-md-3 col-xs-3" style="height:30px; margin-left: -18px; margin-right: 9px">
				Preferred Subject:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	
							<select class="fa fa-" name="s_subject" required="1">

						<option >Physcis</option>

						<option >Chemistry</option>

						<option >Biology</option>

						<option >Maths</option>

					</select>

			 </div>
		</div><br>
		
		<!-- <div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
					Area of Interest 1:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<select class="fa fa-" name="s_aoi1"  >
								<option >A</option>
								<option >B</option>
								<option >C</option>
								<option >D</option>
							</select>

			 </div> -->
		<!-- </div><br> -->
		
		<!-- <div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
				Area of Interest 2:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<select class="fa fa-" name="s_aoi2" >
								<option >A</option>
								<option >B</option>
								<option >C</option>
								<option >D</option>
							</select>

			 </div>
		</div><br> -->

				
				

					<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;">
				 
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			        <button class="button" class="fa fa-" type="submit">Register</button>
			 	  
			 </div>
		</div><br>

					<!-- <div class="col-xs-12" > -->
			         
			        <!-- <button class="button" class="fa fa-" type="submit" style="margin-left: 200px;">Register</button> -->
			        <!-- <button class="button" class="fa fa-" href="login.php">Login</buton>  -->
			       </div>
			      </form>
				</div>
			</div>
			<div class="col-md-4 s_right">

				<!-- registration form starts here -->
		<div class="col-xs-4 registration" style="border: 1px solid">
			<i class="fa fa-close" style="cursor:pointer;color: red;size: 10px;"></i>
		


			<form  method="POST" action="../basic_html/login1.php">

	  <center>        
	  		 		<div class="col-xs-12" style="padding: 10px; ">
	  		 			<h2 style="font-family: times new roman">Please Login</h2>
	  		 		</div>
	  		 		<div class="col-xs-12">
	  		         <div class=" col-xs-4 col-md-4"> 
						EMAIL:
					</div> 
					<div class=" col-xs-8 col-md-8" >
						<input type="text" name="s_email"  placeholder="EMAIL" class="email" required="1" style="width: 200px">
					</div>
					</div>
					<div class="col-xs-12" style="padding: 10px;">
					<div class=" col-xs-4 col-md-4"> 
						PASSWORD:
					</div> 
					<div class=" col-xs-8 col-md-8">
						<input type="password" name="s_password" id="inputPassword" class="email" placeholder="Password" required="1" style="height: 30px;">
					</div>
					</div>
					
					<div class=" col-xs-4 col-md-4"> 
						<a href="forgetpass.html">Forget Password</a>
					</div>

      			 <!-- <center><button class="button" class="fa fa-" type="submit" style="margin-top: 25px;">Register</button> -->
      	 	<div class="col-xs-12">
        		<button class="button" style="margin-top: 25px; class="fa fa-" type="submit" >Login</buton>
        	</div>
       </center>
</form>
		</div>
	<!-- registration form ends here -->
        	</div>
			</div>
		 </div>
	</div>
	<script>
			$(document).ready(function(){
				$('.LoginNow').click(function(){
					$('.registration').addClass('showForm');
				});
				$('.registration i').click(function(){
					$('.registration').removeClass('showForm');
				});
				$('.s_left li').hover(function(){
					$(this).find('a').addClass('white');
				},
				function(){
					$(this).find('a').removeClass('white');
				}

				);
		
				$(window).scroll(function(){
					if($(this).scrollTop() > 0){
						$('.header').addClass('sticky');
					}
					else{
						$('.header').removeClass('sticky');
					}
				});


	});
	
	

	
	
</script>

	

</body>

</html>


<?php
}
else
{
    echo "GO and LOGIN first";
}
?>