<?php

include("../../../process/basic_process/connect.php");

if(!empty($_POST["logout"])) {
	$_SESSION["s_email"] = "";
	session_destroy();
	header("Location: ../../basic_html/baap.php");
	die();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>VSRP</title>
		 <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">


</head>
<body>


<div style="position:fixed; background-color:white;z-index:99;">
<div class="col-xs-12 container-fluid">
		<div class="col-md-12 col-xs-12 s_header">
			<div class="col-md-3 col-xs-3 s_logo">
				<img src="tifr_logo.png" style="width: 140px;height: 65px" />
			</div>	
			<div class="s_content">
				<div class="logo_text">Tata Institute of Fundamental Research</div>
				<div class="header_text">Visiting Student's Research Programme - VSRP-2018</div>
			</div>
		</div>

		<div class="col-xs-12 nav_bar">
			<ul class="col-xs-12 nav_bar">
					<li><a class="fa fa-" href="website.php">HOME</a></li>
					<li><a class="fa fa-" href="#">STUDENT LOGIN</a></li>
					<li><a class="fa fa-" href="#">DEPARTMENT LOGIN</a></li>
					<li><a class="fa fa-" href="#">ADMIN LOGIN</a></li>
					<li><a class="fa fa-" href="#">ABOUT US</a></li>
					<li><form method="post" action="logout.php"><button name="logout" style="    width: 100px;
    background-color: #F6223F;
    text-align: center;
    color: White;
    "><a class="fa fa-">Logout</a></button></form></li>
				    
					
					
				</ul>
		</div>


<!-- start of important marquee -->
<table id="top3" style="width:100%;font-size:14px;font-family: Verdana;border-collapse:collapse;">
		<tbody><tr>
		<td style="width:100px;background-color:#F6223F;text-align:center;color:White;font-weight:bold;">IMPORTANT</td>
		<td style="background-color:rgba(246, 34, 63, 0.1);color:Black;"><div class="important-text">
		<marquee id="ctl00_rightContainer_ContentTable2_scrollerb" direction="alternate" scrollamount="2" scrolldelay="20" style="padding-left: 30px;" onmouseover="javascript:this.stop();" onmouseout="javascript:this.start();">
		 		<!--Repeating-->
		 
		<i style="font-size: 8px;vertical-align: middle;" class="fa fa-circle"></i> 
		<lang key="D1745">Do not admit  any candidates in Against CAP vacancy till further instructions. Check Notification dated 18.08.2018 regarding Additional round.(19-08-2018)
                                                            <blink><font color="RED"><b>New!</b></font></blink>
																</lang>

		<!--Repeating End-->

				<!--Repeating-->
		 
		<i style="font-size: 8px;vertical-align: middle;" class="fa fa-circle"></i> 
		<lang key="D1745">Caste/Tribe Validity Certificate not required for Diploma admissions(07-07-2018)
                            								</lang>

		<!--Repeating End-->

				<!--Repeating-->
		 
		<i style="font-size: 8px;vertical-align: middle;" class="fa fa-circle"></i> 
		<lang key="D1745">NRI/PIO/OCI/CIWGC/Foreign National candidates : For academic year 2018-19 No seats sactioned by AICTE for Diploma Engineering and Technology Programmes, hence Link is not made available to register such candidates in Academic year 2018-19.(27-06-2018)
                            								</lang>

		<!--Repeating End-->

				</marquee>
		</div>
		</td>
		</tr>
		</tbody>
		</table>
	</div>
	</div>

</body>
</html>