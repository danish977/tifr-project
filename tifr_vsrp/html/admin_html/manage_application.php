<?php
session_start();

if(isset($_SESSION['admin_email']))
{
    
?>


<!DOCTYPE html>
<html>
<head>
	<title>Manage Application</title>
	


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">
    
    <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<style>
h5 {
    background-color: #0a1d53 !important;
    color: #fff;
    padding: 9px;
    font-weight: bold;
    font-family: verdana;
    font-size: 11px;
    text-transform: uppercase;
    color: #fff;
    width: 100%;
    margin-top: 1px;
}

#link_1{
    background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}

    
</style>


</head>
<body>
    
    <script type="text/javascript">
$(document).ready(function(){
    $('#dept').on('change',function(){
        var deptID = $(this).val();
        if(deptID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'dept_id='+deptID,
                success:function(html){
                    $('#aoi1').html(html);
                    $('#aoi2').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#aoi1').html('<option value="">Select country first</option>');
            $('#aoi2').html('<option value="">Select state first</option>'); 
        }
    });
    
    $('#dept').on('change',function(){
        var deptID = $(this).val();
        if(deptID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'aoi1_id='+deptID,
                success:function(html){
                    $('#aoi2').html(html);
                }
            }); 
        }else{
            $('#aoi2').html('<option value="">Select state first</option>'); 
        }
    });
});
</script>
    
    <h5>Search Application</h5>
	<form method="POST" action="filter.php">
	    
	<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
				Gender:
 </div> 

			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_gender" id="s_gender">
                  <option class="fa fa-" name="s_gender" value="m">MALE</option>
                  <option class="fa fa-" name="s_gender" value="f">FEMALE</option>
                  <option class="fa fa-" name="s_gender" value="o">OTHERS</option>
                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			      Name:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="app_name" > 
			 </div>
		</div><br>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			     	Registration Number:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="app_regno"  > 
			 </div>
		</div><br>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Email			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="email" class="fa fa-" name="app_email" > 
			 </div>
		</div><br>
		<?php
		
include('../../process/basic_process/connect.php');
include('dbConfig.php');
    
    //Get all country data
    $query = $db->query("SELECT * FROM countries WHERE status = 1 ORDER BY dept_name ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    ?>
         	
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
    Department
        </div>
         <div class="col-md-9 col-xs-9">
             <select name="dept" id="dept" > 
        <option value="">Select Country</option>
        
        <?php
        if($rowCount > 0){
            while($row = $query->fetch_assoc()){ 
                echo '<option value="'.$row['dept_id'].'">'.$row['dept_name'].'</option>';
            }
        }else{
            echo '<option value="">Country not available</option>';
        }
        ?>
    </select>
    </div> 
        	</div><br>
    	<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
    Area of Interest
        </div>
         <div class="col-md-9 col-xs-9">
    <select name="aoi1" id="aoi1">
        <option value="">Select country first</option>
    </select>
      </div> 
        	</div><br>
        		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
    Area of Interest
        </div>
        <div class="col-md-9 col-xs-9">
    <select name="aoi2" id="aoi2">
        <option value="">Select state first</option>
    </select>
 
			   </div> 
        	</div><br>	
 		
		
		
 		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
				Refree Reports Received:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_rrr" id="s_rrr">
                  <option class="fa fa-" name="s_rrr" value="0">0</option>
                  <option class="fa fa-" name="s_rrr" value="1">1</option>
                  <option class="fa fa-" name="s_rrr" value="2">2</option>
                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Numbers of years of Undergrad Students: </div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_us" id="s_us">
                  <option class="fa fa-" name="s_us" value="0">0</option>
                  <option class="fa fa-" name="s_us" value="1">1</option>
                  <option class="fa fa-" name="s_us" value="2">2</option>
                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Numbers of years of PostGradute Students: </div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_ps" id="s_ps">
                  <option class="fa fa-" name="s_ps" value="0">0</option>
                  <option class="fa fa-" name="s_ps" value="1">1</option>
                  <option class="fa fa-" name="s_ps" value="2">2</option>
                </select>
			 </div>
		</div>
		
			<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Status:</div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_status" id="s_status">
                  <option class="fa fa-" name="s_status" value="Selected">Selected</option>
                  <option class="fa fa-" name="s_status" value="Applied">Applied</option>
                  <option class="fa fa-" name="s_status" value="Wait Listed">Wait Listed</option>
                   <option class="fa fa-" name="s_status" value="Basic">Basic</option>

                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			     	Scroll:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="s_scroll"  > 
			 </div>
		</div><br>
		
		
		<input type="submit" value="Submit" style="margin-left:23px;">	
		<input type="reset" value="Reset">
			</form>

	        	<br>	
	<div>
		    <h5>Application List:</h5>
		    
			     	Total Records:

			 
			 	<?php 
			 	$query= "SELECT * from vsrp_student_main";
			 	$result= mysqli_query($con,$query);
 			$rowcount=mysqli_num_rows($result);
 			$row = mysqli_fetch_all($result,MYSQLI_ASSOC);
 			 
 			 echo $rowcount;
 			 //print_r($row);
 			 $i2=0;?>
 			 
 			     
 			      <html>
        <head>
<style>
th{
   background-color:#b0dfd4;
   padding:5px;
}

td{
    padding:5px;
}
</style>
</head>
<body>
 			     
<table width='100%'>
  <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>DEPTCHOICE</th>
        <th>PHOTO</th>
        <th>COLLEGE</th>
        <th>CLASS</th>
        <th>R2DEPT</th>
        <th>R2TEL</th>
        <th>R1DES</th>
        <th>ADDINFO</th>

  </tr>
 <?php
 while($i2< $rowcount){
 	echo "	<tr>
 		    
 		<td>". $row[$i2]['s_main_id']."</td>
 			<td>". $row[$i2]['s_name']."</td>
 			<td>". $row[$i2]['s_deptchoice']."</td>
 			<td> <img src=../../../main_form/".$row[$i2]['s_photo']." height=50px width=50px></td>
 			<td>". $row[$i2]['s_ocollege']."</td>
 	     	<td>". $row[$i2]['s_oclass']."</td>
 			<td>". $row[$i2]['s_r2dept']."</td>
 			<td>". $row[$i2]['s_r2tel']."</td>
 			<td>". $row[$i2]['s_r1des']."</td>
 			<td>". $row[$i2]['s_addinfo']."</td>
 		</tr> 
 	    <td id='link_1'style='background-color:#F5F5F5;'><form method='POST' action='../../process/basic_process/delete.php'><input type='hidden' name='s_main_id' value=".$row[$i2]['s_main_id']."><input type='submit' value='Delete Application' name='delete_application' style='border:none;'></form></td> 
 		<td id='link_1'style='background-color:#F5F5F5;'><form method='POST' action='../../pdf/pdfform.php'><input type='hidden' name='s_email' value=".$row[$i2]['s_email']."><input type='submit' value='Download PDF' name='download_pdf'></form></td>
 		<td id='link_1'style='background-color:#F5F5F5;'><a href='#'>Send Download PDF Link</a></td> 
 		<td id='link_1'style='background-color:#F5F5F5;'><form method='POST' action='stdform/stdform1.php'><input type='hidden' name='s_email' value=".$row[$i2]['s_email']."><input type='submit' value='Edit Application' name='edit_application'></form></td> 
 		<td id='link_1'style='background-color:#F5F5F5;'><a href='#'>Send Refree Link</a></td> 
 		<td id='link_1' style='background-color:#F5F5F5;'><a href='#'>View Application</a></td>
 		";
 		
 			 ?>
 		
 			     
        <?php  
        $i2++;
 			 }
			 	?>
			 	
			 	</table>
			 	<div class="col-xs-12" style="padding:2px; background-color:#b0dfd4;">
		
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_ps" id="s_rrra">
                  <option class="fa fa-" name="s_rrra" value="Refree Report Recieved Acknowledgment">Refree Report Recieved Acknowledgment</option>
                
                </select>
                <input type="Update" value="Update" style="text-align:center;">

			 </div>
		</div>
		
			 		
 			     
 			     </body>
        </html>
			
	
		
<?php
}
else
{
    echo "GO and LOGIN first";
}
?>	
