<?php
//Include database configuration file
include('dbConfig.php');

if(isset($_POST["dept_id"]) && !empty($_POST["dept_id"])){
    //Get all state data
    $query = $db->query("SELECT * FROM states WHERE dept_id = ".$_POST['dept_id']." AND status = 1 ORDER BY aoi1_name ASC");

    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //Display states list
    if($rowCount > 0){
        echo '<option value="">Select state</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['aoi1_id'].'">'.$row['aoi1_name'].'</option>';
        }
    }else{
        echo '<option value="">State not available</option>';
    }
}

if(isset($_POST["aoi1_id"]) && !empty($_POST["aoi1_id"])){
    //Get all city data
    $query = $db->query("SELECT * FROM cities WHERE dept_id = ".$_POST['aoi1_id']." AND status = 1 ORDER BY aoi2_name ASC");
    
    //Count total number of rows
    $rowCount = $query->num_rows;
    
    //Display cities list
    if($rowCount > 0){
        echo '<option value="">Select city</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['aoi2_id'].'">'.$row['aoi2_name'].'</option>';
        }
    }else{
        echo '<option value="">City not available</option>';
    }
}
?>