<?php
session_start();

if(isset($_SESSION['admin_email']))
{
    
?>

<!DOCTYPE html>
<html>
<head>
	<title>Manage Application</title>
	 <script type="text/javascript">
$(document).ready(function(){
    $('#app_dept').on('change',function(){
        var deptID = $(this).val();
        if(deptID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'a_did='+deptID,
                success:function(html){
                    $('#aoi1').html(html);
                }
            }); 
        }else{
            $('#aoi1').html('<option value="">Select dept first</option>');
        }
    });
    
    $('#aoi1').on('change',function(){
        var aoi1ID = $(this).val();
        if(aoi1ID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'a_id='+aoi1ID,
                success:function(html){
                    $('#aoi1').html(html);
                }
            }); 
        }else{
            $('#aoi1').html('<option value="">Select state first</option>'); 
        }
    });
});
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">
    
    <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<style>
h5 {
    background-color: #0a1d53 !important;
    color: #fff;
    padding: 9px;
    font-weight: bold;
    font-family: verdana;
    font-size: 11px;
    text-transform: uppercase;
    color: #fff;
    width: 100%;
    margin-top: 1px;
}

.error {color: #FF0000;}



    
</style>


</head>
<body>
    
    <h5>Manual Registration</h5>
	<form method="POST" action="manageapp.php">
	    
		
		<p><span class="error">All Fields marked * are Mandotary</span></p>
	<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
					Title:
 </div> 

			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_title" id="s_title">
                  <option class="fa fa-" name="s_title" value="m">MALE</option>
                  <option class="fa fa-" name="s_title" value="f">FEMALE</option>
                  <option class="fa fa-" name="s_title" value="o">OTHERS</option>
                </select>						

			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			      First Name:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="s_fname" required="1"  > 
			 </div>
		</div><br>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			     	Middle Name:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="s_mname" required="1"  > 
			 </div>
		</div><br>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			     	Surname:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="s_sname" required="1"  >
			 </div>
		</div><br>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
					Email
			</div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="email" class="fa fa-" name="app_email" required="1"  > 
			 </div>
		</div><br>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
				Subject:
 </div> 

			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_subject" id="s_subject">
                  <option class="fa fa-" name="s_subject" value="m">MALE</option>
                  <option class="fa fa-" name="s_subject" value="f">FEMALE</option>
                  <option class="fa fa-" name="s_subject" value="o">OTHERS</option>
                </select><p><span class="error">*</span></p>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
				Degree:
 </div> 

			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_degree" id="s_degree">
                  <option class="fa fa-" name="s_degree" value="m">MALE</option>
                  <option class="fa fa-" name="s_degree" value="f">FEMALE</option>
                  <option class="fa fa-" name="s_degree" value="o">OTHERS</option>
                </select><p><span class="error">*</span></p>
			 </div>
		</div>
		
		

 		<?php
 			
			include("../../process/basic_process/connect.php");
 			$query= "SELECT * from admin_department";
 			$result= mysqli_query($con,$query);
 			$rowcount=mysqli_num_rows($result);
 			$row = mysqli_fetch_all($result,MYSQLI_ASSOC);
 			?>
 			
 				<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			      Department:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	
 			<select name="app_dept" id="app_dept">
 				<option value="" hidden>Select</option>
 				<?php
 						if($rowcount>0){
 							$i = 0;
 							while ($i < mysqli_num_rows($result) ) {
 								echo '<option value="'.$row[$i]['a_did'].'">'.$row[$i]['a_dname'].'</option>';
 								$i++;
 								
 							}
 						}else{
 							echo '<option value="">Department not available</option>';
 						}
 				?>
 			</select>
 			
 			</div>
		</div><br>
 			
 		<?php	//print_r($row); ?>
 		
 		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			      	Area of Interest 1: 
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
                    <select name="ao1" id="aoi1">
      				  				<option value="" hidden>Select department first</option>
    				</select>			
		   	 </div>
		</div><br>
 		
 	 
 		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			      	Area of Interest 2: 
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
<select name="ao1" id="aoi1">
      				  				<option value="" hidden>Select department first</option>
    						</select>			 </div>
		</div><br>
		
		
 		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
				Refree Reports Received:
 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_rrr" id="s_rrr">
                  <option class="fa fa-" name="s_rrr" value="0">0</option>
                  <option class="fa fa-" name="s_rrr" value="1">1</option>
                  <option class="fa fa-" name="s_rrr" value="2">2</option>
                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Numbers of years of Undergrad Students: </div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_us" id="s_us">
                  <option class="fa fa-" name="s_us" value="0">0</option>
                  <option class="fa fa-" name="s_us" value="1">1</option>
                  <option class="fa fa-" name="s_us" value="2">2</option>
                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Numbers of years of PostGradute Students: </div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_ps" id="s_ps">
                  <option class="fa fa-" name="s_ps" value="0">0</option>
                  <option class="fa fa-" name="s_ps" value="1">1</option>
                  <option class="fa fa-" name="s_ps" value="2">2</option>
                </select>
			 </div>
		</div>
		
			<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
Status:</div> 
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_status" id="s_status">
                  <option class="fa fa-" name="s_status" value="Selected">Selected</option>
                  <option class="fa fa-" name="s_status" value="Applied">Applied</option>
                  <option class="fa fa-" name="s_status" value="Wait Listed">Wait Listed</option>
                   <option class="fa fa-" name="s_status" value="Basic">Basic</option>

                </select>
			 </div>
		</div>
		
		<div class="col-xs-12" style="padding: 10px;">
			<div class="col-md-3 col-xs-3" style="height:30px;padding-top:5px;">
			     	Scroll:
			 </div> 
			 <div class="col-md-9 col-xs-9"> 
			 	<input type="text" class="fa fa-" name="s_scroll" required="1"  > 
			 </div>
		</div><br>
		
		
		<input type="submit" value="Submit" style="margin-left:23px;">	
		<input type="reset" value="Reset">
			</form>

	        	<br>	
	<div>
		    <h5>Application List:</h5>
		    
			     	Total Records:

			 
			 	<?php 
			 	$query= "SELECT * from vsrp_student_main";
			 	$result= mysqli_query($con,$query);
 			$rowcount=mysqli_num_rows($result);
 			$row = mysqli_fetch_all($result,MYSQLI_ASSOC);
 			 
 			 echo $rowcount;
 			 //print_r($row);
 			 $i2=0;?>
 			 
 			     
 			      <html>
        <head>
<style>
th{
   background-color:#b0dfd4;
   padding:5px;
}

td{
    padding:5px;
}
</style>
</head>
<body>
 			     
<table width='100%'>
  <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>DEPTCHOICE</th>
        <th>PHOTO</th>
        <th>COLLEGE</th>
        <th>CLASS</th>
        <th>R2DEPT</th>
        <th>R2TEL</th>
        <th>R1DES</th>
        <th>ADDINFO</th>

  </tr>
 <?php
 while($i2< $rowcount){
 	echo "	<tr>
 		    
 		<td>". $row[$i2]['s_main_id']."</td>
 			<td>". $row[$i2]['s_name']."</td>
 			<td>". $row[$i2]['s_deptchoice']."</td>
 			<td> <img src=../../../main_form/".$row[$i2]['s_photo']." height=50px width=50px></td>
 			<td>". $row[$i2]['s_ocollege']."</td>
 	     	<td>". $row[$i2]['s_oclass']."</td>
 			<td>". $row[$i2]['s_r2dept']."</td>
 			<td>". $row[$i2]['s_r2tel']."</td>
 			<td>". $row[$i2]['s_r1des']."</td>
 			<td>". $row[$i2]['s_addinfo']."</td>
 		</tr> 
 	    <td style='background-color:#F5F5F5;'><a href='#'>Delete Application</a></td> 
 		<td style='background-color:#F5F5F5;'><a href='#'>Download PDF</a></td>
 		<td style='background-color:#F5F5F5;'><a href='#'>Send Download PDF Link</a></td> 
 		<td style='background-color:#F5F5F5;'><a href='#'>Edit Application</a></td> 
 		<td style='background-color:#F5F5F5;'><a href='#'>Send Refree Link</a></td> 
 		<td style='background-color:#F5F5F5;'><a href='#'>View Application</a></td>
 		";
 		
 			 ?>
 		
 			     
        <?php  
        $i2++;
 			 }
			 	?>
			 	
			 	</table>
			 	<div class="col-xs-12" style="padding:2px; background-color:#b0dfd4;">
		
			 <div class="col-md-9 col-xs-9"> 
			 <select name="s_ps" id="s_rrra">
                  <option class="fa fa-" name="s_rrra" value="Refree Report Recieved Acknowledgment">Refree Report Recieved Acknowledgment</option>
                
                </select>
                <input type="Update" value="Update" style="text-align:center;">

			 </div>
		</div>
		
			 		
 			     
 			     </body>
        </html>
			
	
		<?php }
		else {
		    echo "GO and LOGIN fisrt";
		}
	?>
		
	