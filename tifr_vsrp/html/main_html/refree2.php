<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>VSRP</title>
	<style>
	.capbox {
	background-color: #92D433;
	border: #B3E272 0px solid;
	border-width: 0px 12px 0px 0px;
	display: inline-block;
	*display: inline; zoom: 1; /* FOR IE7-8 */
	padding: 8px 40px 8px 8px;
	}

.capbox-inner {
	font: bold 11px arial, sans-serif;
	color: #000000;
	background-color: #DBF3BA;
	margin: 5px auto 0px auto;
	padding: 3px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	}

#CaptchaDiv {
	font: bold 17px verdana, arial, sans-serif;
	font-style: italic;
	color: #000000;
	background-color: #FFFFFF;
	padding: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	}

#CaptchaInput { margin: 1px 0px 1px 0px; width: 135px; }


	</style>
		 <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">
    
    <?php
            
            include("../basic_form/connect.php");
     $email = $_SESSION["s_email"];
    $que= "SELECT * from vsrp_student_main where s_email='$email';";
                    $result= mysqli_query($con,$que);
                    $row=mysqli_fetch_array($result,MYSQLI_ASSOC);
                    $s_r2add1 = $row['s_r2add'];
                
                if(isset($s_r2add1)){
                    $s_r2name1 = $row["s_r2name"];
                    $s_r2des1 = $row["s_r2des"];
                    $s_r2dept1 = $row["s_r2dept"];
                    $s_r2inst1 = $row["s_r2inst"];
                    $s_r2tel1 = $row["s_r2tel"];
                    $s_r2mobno1 = $row["s_r2mobno"];
                    $s_r2email1 = $row["s_r2email"];
                    $s_r2add1 = $row["s_r2add"];
                    $s_addinfo1 = $row["s_addinfo"];
                   
                    




                }else
                {
                     $s_r2name1 = "";
                    $s_r2des1 = "";
                    $s_r2dept1 = "";
                    $s_r2inst1 = "";
                    $s_r2tel1 = "";
                    $s_r2mobno1 = "";
                    $s_r2email1 = "";
                    $s_r2add1 = "";
                    $addinfo1= "";
                }
    ?>



</head>
<body>
    
    <form class="col-xs-12 col-md-12" method="POST" action="../main_form/save_refree2.php" target="_blank"  onsubmit="return checkform(this);">


		<div class="col-md-12">
			<div class="col-md-12 col-xs-12">
				<b> Referee 2 : </b>
			</div>&nbsp;
		</div>
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Name 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2name" placeholder="Enter Referee's Name" required="1" value="<?php echo"$s_r2name1";?>">
			</div> 
		</div>	
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Designation 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2des" placeholder="Enter Referee's Designation" required="1" value="<?php echo"$s_r2des1";?>">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Department
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2dept" placeholder="Enter Referee's Department" required="1" value="<?php echo"$s_r2dept1";?>">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Institution 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2inst" placeholder="Enter Referee's Institution Name" required="1" value="<?php echo"$s_r2inst1";?>">
			</div> 
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Telephone
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_r2tel" placeholder="Enter Referee's Telephone Number" required="1" value="<?php echo"$s_r2tel1";?>">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Mobile No.
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_r2mobno" placeholder="Enter Referee's Mobile Number" required="1" value="<?php echo"$s_r2mobno1";?>">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Email 
			</div>
			<div class="col-md-4 col-xs-12">
				 <input type="email" class="fa fa-" name="s_r2email" required="1" placeholder="Enter Referee's Email" value="<?php echo"$s_r2email1";?>"> 	 
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Address
			</div>
			<div class="col-md-4 col-xs-12">
				<textarea style="margin-left: 15px;" class="form-control" id="comments" placeholder="Enter Your Referee's Address" rows="6" cols="35" name="s_r2add" required="1" value=""><?php echo"$s_r2add1";?></textarea><br>
				<!-- <textarea rows="6" cols="35" placeholder="Enter Referee's Address." name="s_r2add">
				</textarea> -->
			</div>
		</div>

		<div class="col-xs-12 col-md-12">
			<p>
				<b>
					Please make sure that you type the email address of the referees correctly.
				</b>
			</p><br>
			<p>
				On successful submission of your form, an email will be sent to your Referees with link to fill the Referee Report Form online.<br><br>
				
				Any additional information about yourself that has not been covered above.<br><br>
				
				Below may include awards, scholarships or any special academic achievements.<br> 
				
				(please avoid special characters such as *#$%&+"'?~)
			</p>
		</div>
			<div class="col-md-4 col-xs-12">
				<textarea style="margin-left: 15px;" class="form-control" id="comments" placeholder="Enter Your additional information" rows="6" cols="35" name="s_addinfo" required="1" value=""><?php echo"$s_addinfo1";?></textarea><br>
				<!-- <textarea rows="6" cols="35" placeholder="Enter Referee's Address." name="s_r2add">
				</textarea> -->
			</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				<p style="margin-left: -15px;">How did you come to know about VSRP:
				</p>			
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_know" required="1">
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				</select>
			</div>
		</div><br>&nbsp;

<!-- 		<div class="col-xs-12 col-md-12">
			Captcha code
		</div>  -->

		<div class="col-md-12 col-xs-12">
			<input type="checkbox" name="s_agree" value="yes" required="1">  I accept and confirm that the information provided above is true to the best of my knowledge 
			and understand that if at any stage of the selection process, it comes to the notice that false information 
			has been furnished or there has been suppression of any factual information, 
			my application for the program will be disqualified.<br>&nbsp;
		</div>
		



		<div class="col-xs-12 col-md-12">
		    	 <table width="400" border="0" align="center" cellpadding="5" cellspacing="1" class="table">
    <?php if(isset($msg)){?>
    <tr>
      <td colspan="2" align="center" valign="top"><?php echo $msg;?></td>
    </tr>
    <?php } ?>
    <tr>
      <td align="right" valign="top"> Validation code:</td>
      <td><img src="captcha.php?rand=<?php echo rand();?>" id='captchaimg'><br>
        <label for='message'>Enter the code above here :</label>
        <br>
        <input id="captcha_code" name="captcha_code" type="text">
        <br>
        Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input name="Submit" type="submit" onclick="return validate();" value="Submit" class="button1"></td>
    </tr>
  </table>
		</div>
</form>	
<script type="text/javascript">

// Captcha Script

function checkform(theform){
var why = "";

if(theform.CaptchaInput.value == ""){
why += "- Please Enter CAPTCHA Code.\n";
}
if(theform.CaptchaInput.value != ""){
if(ValidCaptcha(theform.CaptchaInput.value) == false){
why += "- The CAPTCHA Code Does Not Match.\n";
}
}
if(why != ""){
alert(why);
return false;
}
}

var a = Math.ceil(Math.random() * 9)+ '';
var b = Math.ceil(Math.random() * 9)+ '';
var c = Math.ceil(Math.random() * 9)+ '';
var d = Math.ceil(Math.random() * 9)+ '';
var e = Math.ceil(Math.random() * 9)+ '';

var code = a + b + c + d + e;
document.getElementById("txtCaptcha").value = code;
document.getElementById("CaptchaDiv").innerHTML = code;

// Validate input against the generated number
function ValidCaptcha(){
var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
var str2 = removeSpaces(document.getElementById('CaptchaInput').value);
if (str1 == str2){
return true;
}else{
return false;
}
}

// Remove the spaces from the entered and generated code
function removeSpaces(string){
return string.split(' ').join('');
}
</script>

    
</body>
</html>