<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>VSRP</title>
		 <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    
    <meta name="viewport" content="width=device-width">
    <?php
    
            include("../basic_form/connect.php");
     $email = $_SESSION["s_email"];
    $que= "SELECT * from vsrp_student_main where s_email='$email';";
                    $result= mysqli_query($con,$que);
                    $row=mysqli_fetch_array($result,MYSQLI_ASSOC);
                   $s_yopg1 = $row['s_yopg'];
                
                if(isset($s_yopg1)){
                    $s_desc1 = $row["s_desc"];
                    $s_eng1 = $row["s_eng"];
                    $s_odeg1 = $row["s_odeg"];
                    $s_osubject1 = $row["s_osubject"];
                    $s_deptchoice1 = $row["s_deptchoice"];
                    $s_aor1 = $row["s_aor1"];
                    $s_aor2 = $row["s_aor2"];
                    $s_dyi1 = $row["s_dyi"];
                    $s_subject1 = $row["s_subject"];
                    $s_colin1 = $row["s_colin"];
                    




                }else
                {
                    $s_desc1 = "";
                    $s_eng1 = "";
                    $s_odeg1 = "";
                    $s_osubject1 = "";
                    $s_deptchoice1 = "";
                    $s_aor1 = "";
                    $s_aor2 = "";
                    $s_dyi1 = "";
                    $s_subject1 = "";
                    $s_colin1 = "";
                    
                }
    
    ?>


</head>
<body>

<form class="col-xs-12 col-md-12" method="POST" action="../main_form/save_educational.php">

		<div class="col-xs-12 col-md-12 educational">
				<h3 style="text-align: left;">Educational Information</h3>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				I am studying for Master`s/Bachelor`s degree in: 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_desc" placeholder="Enter Description" required="1" value="<?php echo "$s_desc1"; ?>">
			</div><br>
		</div><br>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Engineering 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_eng" placeholder="Specify Branch" required="1" value="<?php echo "$s_eng1";?>">
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Degrees
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_odeg" required="1" value="<?php echo "$s_odeg1"; ?>">
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Subject
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_osubject" placeholder="Other Subject" required="1" value="<?php echo "$s_osubject1"; ?>">
			</div>
		</div><br>&nbsp;

		<p style="padding-left: 32px;">I would like to be considered for the following Department : </p>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Department choice
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_deptchoice" required="1" value="<?php echo "$s_deptchoice1"; ?>">
				<option >Physcis</option>
				<option >Chemistry</option>
				<option >Biology</option>
				<option >Maths</option>
				</select>
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Area of Research 1
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_aor1" required="1" value="<?php echo "$s_aor1"; ?>">
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				</select>
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Area of Research 2
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_aor2" required="1"  value="<?php echo "$s_aor2"; ?>">
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				</select>
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				<strong>Note :</strong> 
			</div>
			<div class="col-md-4 col-xs-12">
				<p>
					In 400 words or less describe your interest in your most preferred research area.<br>
					The write-up should particularly address questions such as:<br>
					(1) Which area in this subject specially appeals to you?<br>
					(2) Are there specific problems you would like to work on?<br>
					(3) Can you suggest methods to deal with such problems?<br>
					<b>WE WOULD LIKE THE WRITE-UP TO REFLECT YOUR ORIGINAL IDEAS</b><br>
				</p>
			</div>
		</div><br>&nbsp;

		<div class="col-xs-12 col-md-12">
			<p><strong>The write up should not contain special characters. In particular, use of equations in write-ups is strongly discouraged.</strong> 
			</p>
		</div>

		<div class="col-xs-12 col-md-4">
			<textarea name="s_dyi" required="1" ><?php echo "$s_dyi1"; ?></textarea><br>


		
		</div>

		<div class="col-xs-12 col-md-12 selsub">
				<h3 style="text-align: left;">Selected Subject and Degree</h3>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Subject
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_subject" required="1" value="<?php echo "$s_subject1"; ?>">
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				College/Institution
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_colin" required="1" value="<?php echo "$s_colin1"; ?>">
			</div>
		</div><br>&nbsp;

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Year of Postgraduate
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_yopg" placeholder="Current Year" required="1" value="<?php echo "$s_yopg1"; ?>" >
			</div>
		</div><br>&nbsp;
	<div class="col-xs-12 col-md-12">	
	<button class="button" type="submit" style="    background-color: #0a1d53;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;">Save</button>
    </div>
		
</form>
</body>
</html>