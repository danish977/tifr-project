<?php

session_start();
include("connect.php");

if(!empty($_POST["logout"])) {
	$_SESSION["s_email"] = "";
	session_destroy();
	header("Location: ../basic_html/login_form.php");
	die();
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Form  2 for VSRP Registration</title>
	<!-- <meta name="viewport" content="width=device-width"> -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes, maximum-scale=1,user-scalable=no">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style1.css">

   <!--  <style type="text/css">
	html,body{
	overflow-x:hidden;
	}
	</style> To  hide the horizontal scrollbar-->  

</head>
<body>

	<div class="col-xs-12">
	<h1 style="text-align: center; color: #096487">VSRP Complete Registration</h1>
	<p style="text-align: center;"><strong>Note:</strong>Please make sure that Adobe Reader is installed in your machine. You can download Adobe reader from http://get.adobe.com/reader/otherversions/</p>

	
		<div class="col-xs-12 col-md-12 form" style="padding: 14px;">
				<h3 style="text-align: left;">Personal Information</h3>
		</div>

	<form class=" form1 col-xs-12 col-md-12" method="POST">
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Name 
			</div>
			<div class="col-md-4 col-xs-12 name">
				<input type="text" class="fa fa-" name="s_fname" placeholder="Enter Full Name">
			</div>
		</div><br>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Email 
			</div>
			<div class="col-md-4 col-xs-12 email">
				 <input type="email" class="fa fa-" name="s_email" required="1" > 	 
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Gender 
			</div>
			<div class="col-md-4 col-xs-12 gender">
				<input type="radio" class="fa fa-" name="s_gender" value="m">MALE<br>
				<input type="radio" class="fa fa-" name="s_gender" value="f">FEMALE<br>
				<input type="radio" class="fa fa-" name="s_gender" value="o">OTHERS<br>
			</div>
		</div>

		<div class="col-md-12"  style="padding: 14px;" >
			<div class="col-md-2 col-xs-12">
				Date of Birth
			</div>
			<div class="col-md-4 col-xs-12 email">
				 <input type="date" class="fa fa-" name="s_dob" >	 
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Birth Place
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_birth">
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Nationality 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_fname">
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Upload Photo
			</div>
			<div class="col-md-4 col-xs-12">
				<label for="avatar">Profile picture (Passport Size):</label>
        		<input type="file"
               class="upload" name="s_upload"
               accept="image/png, image/jpeg" />
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Permanent Postal Address
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_add" placeholder="Room Number,Locality"><br>
				<input type="text" class="fa fa-" name="s_add" placeholder="Landmark"><br>
				<input type="text" class="fa fa-" name="s_add" placeholder="Area"><br>
				<input type="text" class="fa fa-" name="s_add" placeholder="City"><br>
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Address For Correspondence 
			</div>
			<div class="col-md-4 col-xs-12 name">
				<input type="text" class="fa fa-" name="s_cadd"><br>
				<input type="text" class="fa fa-" name="s_cadd">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Telephone
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_tel" >
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Mobile No.
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_mobno" >
			</div>
		</div>




		<div class="col-xs-12 col-md-12 educational">
				<h3 style="text-align: left;">Educational Information</h3>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				I am studying for Master`s/Bachelor`s degree in: 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_des" placeholder="Enter Description" >
			</div><br>
		</div><br>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Engineering 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_eng" placeholder="Specify Branch" >
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Degrees
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_deg" >
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Subject
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_deg" placeholder="Other Subject" >
			</div>
		</div><br>&nbsp

		<p style="padding-left: 32px;">I would like to be considered for the following Department : </p>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Department choice
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_subject">
				<option >Physcis</option>
				<option >Chemistry</option>
				<option >Biology</option>
				<option >Maths</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Area of Research 1
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_aor1" required="1" >
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Area of Research 2
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_aor2"  >
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				<strong>Note :</strong> 
			</div>
			<div class="col-md-4 col-xs-12">
				<p>
					In 400 words or less describe your interest in your most preferred research area.<br>
					The write-up should particularly address questions such as:<br>
					(1) Which area in this subject specially appeals to you?<br>
					(2) Are there specific problems you would like to work on?<br>
					(3) Can you suggest methods to deal with such problems?<br>
					<b>WE WOULD LIKE THE WRITE-UP TO REFLECT YOUR ORIGINAL IDEAS</b><br>
				</p>
			</div>
		</div><br>&nbsp

		<div class="col-xs-12 col-md-12">
			<p><strong>The write up should not contain special characters. In particular, use of equations in write-ups is strongly discouraged.</strong> 
			</p>
		</div>

		<div class="col-xs-12 col-md-12">
			<textarea rows="6" cols="35" maxlength="400" placeholder="400 words only">
			</textarea>
			 <!-- <input type="text" name="textarea"maxlength="400" placeholder="400 words only" style="    height: 100px; width: 300px"> -->
		</div>

		<div class="col-xs-12 col-md-12 selsub">
				<h3 style="text-align: left;">Selected Subject and Degree</h3>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Subject
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_sub"  >
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				College/Institution
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_colinst" >
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Year of Postgraduate
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_ypost" placeholder="Current Year" >
			</div>
		</div><br>&nbsp


		<div class="col-xs-12 col-md-12 selsub">
				<h3 style="text-align: left;">Qualifications</h3>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Select Degree
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_sdeg" required="1" >
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				<option >E</option>
				<option >F</option>
				<option >G</option>
				<option >H</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				College 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_colg" placeholder="Enter College Name">
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Board/University 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_univ" placeholder="Enter University/Board Name">
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Year
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_year" required="1" >
				<option >2015</option>
				<option >2016</option>
				<option >2017</option>
				<option >2018</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Class
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_class"  >
				<option >First Yaer</option>
				<option >Second Year</option>
				<option >Third Year</option>
				<option >Fourth year</option>
				</select>
			</div>
		</div><br>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Marks in CGPA/Percentage 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_marks" >
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				University Rank
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_univrank"  >
				<option >1</option>
				<option >2</option>
				<option >3</option>
				<option >4</option>
				</select>
			</div>
		</div><br>&nbsp


		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Science/Engg Subjects
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_subj" placeholder="Enter Subject Names">
			</div>
		</div><br>&nbsp



		<div class="col-xs-12 col-md-12 selsub">
				<h4 style="text-align: left;">Other Qualifications</h4>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Degree
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_odeg" required="1" >
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				<option >E</option>
				<option >F</option>
				<option >G</option>
				<option >H</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other College 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_ocolg" placeholder="Enter other College Name">
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Board/University 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_ouniv" placeholder="Enter other University/Board Name">
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Year
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_year" required="1" >
				<option >2015</option>
				<option >2016</option>
				<option >2017</option>
				<option >2018</option>
				</select>
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Class
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_aor2"  >
				<option >First Yaer</option>
				<option >Second Year</option>
				<option >Third Year</option>
				<option >Fourth year</option>
				</select>
			</div>
		</div><br>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Other Marks in CGPA/Percentage 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_marks" >
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				University Rank
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_aor2"  >
				<option >1</option>
				<option >2</option>
				<option >3</option>
				<option >4</option>
				</select>
			</div>
		</div><br>&nbsp


		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Other Subjects
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_subj" placeholder="Enter other Subject Names">
			</div>
		</div><br>&nbsp

		<!-- <div class="col-xs-12 col-md-12">
			<p style="padding-left:11px;">When do you expect to complete M.Sc/B.Sc.(Engg)/B.E/B.Tech/M.E/M.Tech/Any other degree(specify)?<br>
			I expect to complete my <input type="text" class="fa fa-" name="s_degtime" placeholder="Enter degree to be completed"> <br>by<br> <input style="margin-left: 152px" type="date" class="fa fa-" name="s_date"  >
			</p>

		</div> -->

		<div class="col-xs-12 col-md-12">
			<p style="padding-left:11px;">When do you expect to complete M.Sc/B.Sc.(Engg)/B.E/B.Tech/M.E/M.Tech/Any other degree(specify)?<br>
			</p>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				I expect to complete my 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_degtime" placeholder="Enter degree to be completed">
			</div>
		</div><br>&nbsp
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Degree in 
			</div>
			<div class="col-md-4 col-xs-12">
				  Month & Year: <input type="month" name="montcompleteh">
			</div>
		</div><br>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-6">
				Publications (if any):
			</div>
			<div class="col-md-4 col-xs-12">
				<p>
					(please avoid special characters such as *#$%&+"'?~,etc.)
				</p>
			</div>
		</div><br>
		<div class="col-xs-12 col-md-12">
			<textarea style="margin-left: 15px;" rows="6" cols="35"  placeholder="Enter Your Publications details if any.....">
			</textarea>

		</div>&nbsp

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Research Experience : <br>(if any)
			</div>
			<div class="col-md-4 col-xs-12">
				<p>
					(please avoid special characters such as *#$%&+"'?~,etc.)
				</p>
			</div>
		</div><br>&nbsp
		<div class="col-xs-12 col-md-12">
			<textarea style="margin-left: 15px;" rows="6" cols="35"  placeholder="Enter Your Research details if any..... ">
			</textarea>
		</div>&nbsp 
		

		<div class="col-md-12 col-xs-12">
			<p style="margin-left: 15px;">
				Names and addresses of two Referees who would fill up the referee reports <br>
				(They should be your teachers or persons with whom you have interacted with academically)
			</p>				
		</div>

		<div class="col-md-12">
			<div class="col-md-12 col-xs-12">
				<b>Referee 1 : </b>
			</div>&nbsp
		</div>
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Name 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r1name" placeholder="Enter Referee's Name" required="1">
			</div> &nbsp 
		</div>	
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Designation 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r1des" placeholder="Enter Referee's Designation" required="1">
			</div>&nbsp
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Department
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r1dep" placeholder="Enter Referee's Department" required="1">
			</div> &nbsp 
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Institution 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r1inst" placeholder="Enter Referee's Institution Name" required="1">
			</div> 
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Telephone
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_r1tel" placeholder="Enter Referee's Telephone Number" >
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Mobile No.
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_r1mobno" placeholder="Enter Referee's Mobile Number">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Email 
			</div>
			<div class="col-md-4 col-xs-12">
				 <input type="email" class="fa fa-" name="s_r1email" required="1" placeholder="Enter Referee's Email" > 	 
			</div>
		</div>

		<div class="col-xs-12 col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Address
			</div>
			<div class="col-md-4 col-xs-12">
				<textarea rows="6" cols="35"  placeholder="Enter Referee's Address.">
				</textarea>
			</div>
		</div>



		<div class="col-md-12">
			<div class="col-md-12 col-xs-12">
				<b> Referee 2 : </b>
			</div>&nbsp
		</div>
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Name 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2name" placeholder="Enter Referee's Name" required="1">
			</div> 
		</div>	
		
		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Designation 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2des" placeholder="Enter Referee's Designation" required="1">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Department
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2dep" placeholder="Enter Referee's Department" required="1">
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Institution 
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="text" class="fa fa-" name="s_r2inst" placeholder="Enter Referee's Institution Name" required="1">
			</div> 
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Telephone
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_r2tel" placeholder="Enter Referee's Telephone Number" >
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Mobile No.
			</div>
			<div class="col-md-4 col-xs-12">
				<input type="number" class="fa fa-" name="s_r2mobno" placeholder="Enter Referee's Mobile Number"  >
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				Email 
			</div>
			<div class="col-md-4 col-xs-12">
				 <input type="email" class="fa fa-" name="s_r2email" required="1" placeholder="Enter Referee's Email" > 	 
			</div>
		</div>

		<div class="col-md-12" style="padding: 14px;">
			<div class="col-md-2 col-xs-12">
				Address
			</div>
			<div class="col-md-4 col-xs-12">
				<textarea rows="6" cols="35" placeholder="Enter Referee's Address.">
				</textarea>
			</div>
		</div>

		<div class="col-xs-12 col-md-12">
			<p>
				<b>
					Please make sure that you type the email address of the referees correctly.
				</b>
			</p><br>
			<p>
				On successful submission of your form, an email will be sent to your Referees with link to fill the Referee Report Form online.<br><br>
				
				Any additional information about yourself that has not been covered above.<br><br>
				
				Below may include awards, scholarships or any special academic achievements.<br> 
				
				(please avoid special characters such as *#$%&+"'?~)
			</p>
			<div class="col-md-12 col-xs-12">
				<textarea  rows="6" cols="35" placeholder="Enter your additional information.">
				</textarea>
			</div>&nbsp
		</div>

		<div class="col-md-12">
			<div class="col-md-2 col-xs-12">
				<p style="margin-left: -15px;">How did you come to know about VSRP:
				</p>			
			</div>
			<div class="col-md-4 col-xs-12">
				<select class="fa fa-" name="s_subject">
				<option >A</option>
				<option >B</option>
				<option >C</option>
				<option >D</option>
				</select>
			</div>
		</div><br>&nbsp

<!-- 		<div class="col-xs-12 col-md-12">
			Captcha code
		</div>  -->

		<div class="col-md-12 col-xs-12">
			<input type="checkbox" name="s_agree" value="yes">  I accept and confirm that the information provided above is true to the best of my knowledge 
			and understand that if at any stage of the selection process, it comes to the notice that false information 
			has been furnished or there has been suppression of any factual information, 
			my application for the program will be disqualified.<br>&nbsp
		</div>

		<div class="col-xs-12 col-md-12">
			<button class="button" class="fa fa-" type="submit">Submit</button>
			<button class="button" class="fa fa-" type="submit">Reset</button> 
			 </div>
		</div>
        <div class="member-dashboard"> You have successfully logged in!<br>
        Click to <input type="button" name="logout" value="submit" class="logout-button">.</div> 













		

	</form>	
	</div>



</body>
</html>
